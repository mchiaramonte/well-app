# BACKEND

Set-Location ./backend/source
pack build gcr.io/testproject-322911/wellapp/backend:latest --builder paketobuildpacks/builder:base
Write-Output "Tagging new image"
docker image tag gcr.io/testproject-322911/wellapp/backend:latest gcr.io/testproject-322911/wellapp/backend:0.0.11
Set-Location ../../


# docker push gcr.io/testproject-322911/wellapp/backend:latest
# docker push gcr.io/testproject-322911/wellapp/backend:0.0.11

# FRONTEND
Set-Location ./frontend/source
pack build gcr.io/testproject-322911/wellapp/frontend:latest --buildpack gcr.io/paketo-buildpacks/nodejs --builder paketobuildpacks/builder:base
Set-Location ../../
# TODO buildpack frontend too
# docker compose build well-frontend
# docker compose push well-frontend


# UPDATE CLUSTER

# kubectl apply -f ./deployment.yaml

# kubectl rollout restart deployment/wellapp-frontend # Redeploys
# kubectl rollout restart deployment/wellapp-backend # Redeploys