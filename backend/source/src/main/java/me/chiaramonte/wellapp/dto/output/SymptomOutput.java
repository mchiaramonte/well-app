package me.chiaramonte.wellapp.dto.output;

import me.chiaramonte.wellapp.model.Consultation;
import me.chiaramonte.wellapp.model.ICDCategory;
import me.chiaramonte.wellapp.model.Patient;

import java.sql.Timestamp;

public class SymptomOutput {

    private String uuid;

    private ICDCategory icdCategory;

    private String description;

    private Timestamp occurrence;

    private Patient patient;

    private Consultation consultation;

}
