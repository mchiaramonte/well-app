package me.chiaramonte.wellapp.mapper;

import me.chiaramonte.wellapp.dto.input.SymptomInput;
import me.chiaramonte.wellapp.dto.output.SymptomOutput;
import me.chiaramonte.wellapp.model.Symptom;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface SymptomMapper {

    SymptomMapper INSTANCE = Mappers.getMapper(SymptomMapper.class);

    Symptom symptomInputToSymptom(SymptomInput dto);


}
