package me.chiaramonte.wellapp.controller;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.dto.input.ICDCategoryInput;
import me.chiaramonte.wellapp.enums.SuggestionType;
import me.chiaramonte.wellapp.model.ICDCategory;
import me.chiaramonte.wellapp.service.impl.ICD10Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("icd10categories")
public class ICDCategoryController {

    @Autowired
    private ICD10Service icd10Service;

//    TODO implement Pageable
    @GetMapping
    List<ICDCategory> filter(@RequestParam(name="query",required = false, defaultValue = "")  String query,
                             @RequestParam(name="size",required = false, defaultValue = "10") Integer size,
                             @RequestParam(name="suggestions",required = false, defaultValue = "frequency") SuggestionType suggestions){

        return icd10Service.filter(query, size);
    }

    @GetMapping("all")
    List<ICDCategory> all(){

        return icd10Service.findAll();
    }

    @PostMapping("bulk")
    Integer createFromList(@RequestBody List<ICDCategoryInput> input){
        return icd10Service.createICD10Codes(input);
    }


}
