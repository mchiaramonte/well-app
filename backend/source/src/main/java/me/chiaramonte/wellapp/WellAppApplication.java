package me.chiaramonte.wellapp;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(servers = @Server(url = "/api/v1"), info = @Info(title="Well App Demo", version = "0.0.1"))
@SpringBootApplication
public class WellAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WellAppApplication.class, args);
	}

}
