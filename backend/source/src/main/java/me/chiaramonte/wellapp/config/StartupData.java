package me.chiaramonte.wellapp.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.dto.input.ICDCategoryInput;
import me.chiaramonte.wellapp.dto.input.PatientInput;
import me.chiaramonte.wellapp.mapper.PatientMapper;
import me.chiaramonte.wellapp.model.Consultation;
import me.chiaramonte.wellapp.model.Patient;
import me.chiaramonte.wellapp.repository.ConsultationRepository;
import me.chiaramonte.wellapp.repository.PatientRepository;
import me.chiaramonte.wellapp.service.impl.ICD10Service;
import me.chiaramonte.wellapp.service.impl.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
public class StartupData implements ApplicationRunner {

    @Autowired
    private PatientService patientService;

    @Autowired
    private ICD10Service icd10Service;


    @Autowired
    private Environment env;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private PatientRepository patientRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("ApplicationRunner#run()");
        if(Arrays.toString(env.getActiveProfiles()).contains("h2_db")){
            try{
                testResourceFile();
                icd10Codes();

            }
            catch (IOException ex){
                log.info(ex.getMessage());
            }
        }
//

    }

    private void testResourceFile() throws IOException {
        File resource = new ClassPathResource("json/patient-and-address.json").getFile();


        TypeReference < List <PatientInput>> typeReference = new TypeReference <> () {};
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new JavaTimeModule());
        List<PatientInput> patientList = mapper.readValue(Files.readAllBytes(resource.toPath()), typeReference);
        if(patientService.filterByFirstNameOrLastName("").isEmpty()){
            createMockPatients(patientList);
        }
    }

    private void icd10Codes() throws IOException {
        File resource = new ClassPathResource("json/icd10_codes.json").getFile();

        TypeReference < List <ICDCategoryInput>> typeReference = new TypeReference <> () {};
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        List<ICDCategoryInput> icd10Codes = mapper.readValue(Files.readAllBytes(resource.toPath()), typeReference);

        if(icd10Service.findAll().isEmpty()){
            icd10Service.createICD10Codes(icd10Codes);
        }

    }

    /** One time mock consultation creation if database is empty **/
    @Transactional
    public List<Patient> createMockPatients(List<PatientInput> patientList){
        List<Patient> savedPatients = new ArrayList<>();
        patientMapper.patientInputListToPatientList(patientList).forEach(p->{

//            PatientAddress savedAddress = patientAddressRepository.save(p.getAddress());
            Patient createdPatient = patientRepository.save(p);



            savedPatients.add(createdPatient);

            for (int i = 0; i < getRandomNumber(5,10); i++) {
                Consultation mockConsult = new Consultation();
                mockConsult.setPatient(createdPatient);
                int month = getRandomNumber(1,12);
                int day = getRandomNumber(1,28);
                int hour = getRandomNumber(7,17);
                int minute = getRandomNumber(0,3)*15;
                mockConsult.setStartDateTime(ZonedDateTime.of(2021,month,day,hour,minute,0,0, ZoneId.of("UTC+01:00")));
                mockConsult.setEndDateTime(ZonedDateTime.of(2021,month,day,hour + 1,minute,0,0, ZoneId.of("UTC+01:00")));
                consultationRepository.save(mockConsult);
            }

        });
        return savedPatients;
    }

    private int getRandomNumber(int min, int max) {
        return (int) (Math.floor((Math.random() * (max - min)) + min));
    }


}
