package me.chiaramonte.wellapp.model;

import io.swagger.v3.oas.annotations.Hidden;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import java.sql.Timestamp;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class User extends BaseEntity {

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate birthday;

    private String phone;

    @Hidden
    private String userRole;

    @ManyToOne
    private PatientAddress address;

    @Hidden
    private String picture;

}
