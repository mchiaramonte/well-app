package me.chiaramonte.wellapp.controllerExceptions;


// See https://www.baeldung.com/spring-rest-openapi-documentation#controlleradvice
// and https://devwithus.com/exception-handling-for-rest-api-with-spring-boot/

import me.chiaramonte.wellapp.model.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

//@RestControllerAdvice(assignableTypes = PatientController.class) // For specific classes
@RestControllerAdvice // Globally
public class GlobalControllerExceptionHandler {


    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ApiError> handlePatientNotFound(RuntimeException ex) {

        List<String> details = new ArrayList<>();
        details.add(ex.getMessage());

        ApiError err = new ApiError(
                ZonedDateTime.now(),
                HttpStatus.NOT_FOUND,
                "Object Not Found" ,
                details);

        return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);

    }
//
    @ExceptionHandler({InvalidInputException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ApiError> handleInvalidInputData(RuntimeException ex) {

        List<String> details = new ArrayList<>();
        details.add(ex.getMessage());

        ApiError err = new ApiError(
                ZonedDateTime.now(),
                HttpStatus.BAD_REQUEST,
                "Invalid Input Data" ,
                details);

        return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
    }
}
