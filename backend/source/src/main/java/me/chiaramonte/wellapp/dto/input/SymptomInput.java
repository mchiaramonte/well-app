package me.chiaramonte.wellapp.dto.input;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SymptomInput {

    private String description;

    private String occurrence;

    private String patientUuid;

    private String consultationUuid;

    private String icdCategoryUuid;

}
