package me.chiaramonte.wellapp.controller;

import me.chiaramonte.wellapp.dto.input.PatientInput;
import me.chiaramonte.wellapp.model.Consultation;
import me.chiaramonte.wellapp.model.Patient;
import me.chiaramonte.wellapp.service.impl.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("patients")
public class PatientController {

    @Autowired
    private PatientService patientService;


    @GetMapping("{patientUuid}")
    Patient getPatientByUuid(@PathVariable String patientUuid){
        return patientService.findOneByUuid(patientUuid);
    }

    @GetMapping("{patientUuid}/consultations")
    List<Consultation> getPatientConsultations(@PathVariable String patientUuid){
        return patientService.findPatientConsultations(patientUuid);
    }

    @GetMapping("filter")
    List<Patient> filterPatientsByName(@RequestParam(name="query", required = false, defaultValue = "") String query){
        return patientService.filterByFirstNameOrLastName(query);
    }



    // TODO Create and Delete
//
    @PostMapping("bulk")
    Iterable<Patient> create(@RequestBody List<PatientInput> inputs){
        return patientService.createMockPatients(inputs);
    }

//
//    @DeleteMapping
//    ResponseEntity<Patient> delete(){
//
//        return new ResponseEntity(new Patient(),HttpStatus.BAD_REQUEST);
//
//    }

}
