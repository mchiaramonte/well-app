package me.chiaramonte.wellapp.repository;

import me.chiaramonte.wellapp.model.Doctor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface DoctorRepository extends CrudRepository<Doctor,Long> {
}
