package me.chiaramonte.wellapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class Consultation extends BaseEntity {

//    why use instant
//    https://stackoverflow.com/questions/36417317/localdatetime-to-zoneddatetime
    private ZonedDateTime startDateTime;

    private ZonedDateTime endDateTime;

    @ManyToOne(fetch = FetchType.EAGER)
    private Patient patient;

    @JsonIgnore
    @ManyToOne
    private Doctor doctor;

    @JsonIgnore
    @OneToMany(mappedBy = "consultation", fetch = FetchType.EAGER)
    @ToString.Exclude
    List<Symptom> symptomList;

}
