package me.chiaramonte.wellapp.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class Doctor extends BaseEntity {

    @OrderBy("startDateTime ASC")
    @OneToMany(mappedBy = "doctor")
    private List<Consultation> consultationList;

}
