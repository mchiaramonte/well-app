package me.chiaramonte.wellapp.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Status {

    @GetMapping("")
    ResponseEntity<String> livelinessProbe(){
        return ResponseEntity.ok("UP");
    }

}
