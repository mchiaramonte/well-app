package me.chiaramonte.wellapp.service;

import me.chiaramonte.wellapp.controllerExceptions.InvalidInputException;
import me.chiaramonte.wellapp.controllerExceptions.NotFoundException;
import me.chiaramonte.wellapp.dto.input.ConsultationInput;
import me.chiaramonte.wellapp.model.Consultation;


import java.util.List;


public interface ConsultationService {

    Consultation findOneByUuid(String uuid)  throws NotFoundException;

    List<Consultation> findAll();

    Consultation create(ConsultationInput consultationInput) throws InvalidInputException;

    Consultation update(ConsultationInput consultationInput) throws InvalidInputException;
}
