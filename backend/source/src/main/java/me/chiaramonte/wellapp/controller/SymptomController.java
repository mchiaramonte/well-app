package me.chiaramonte.wellapp.controller;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.dto.input.SymptomInput;
import me.chiaramonte.wellapp.model.Symptom;
import me.chiaramonte.wellapp.service.impl.SymptomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("symptoms")
public class SymptomController {

    @Autowired
    private SymptomService symptomService;

    @PostMapping
    Symptom createSymptom(@RequestBody SymptomInput symptomInput){
        return symptomService.create(symptomInput);
    }

    @DeleteMapping("{uuid}")
    Symptom deleteSymptom(@PathVariable String uuid){
        log.info(uuid);
        return symptomService.delete(uuid);
    }

}
