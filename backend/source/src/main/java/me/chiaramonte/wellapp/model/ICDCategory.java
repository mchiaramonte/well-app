package me.chiaramonte.wellapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class ICDCategory extends BaseEntity {

    private String code;

    private String description;

    private Long frequency;

    @JsonIgnore
    @OneToMany(mappedBy = "icdCategory")
    private List<Symptom> symptomList;


}
