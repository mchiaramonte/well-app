package me.chiaramonte.wellapp.mapper;

import me.chiaramonte.wellapp.dto.input.PatientInput;
import me.chiaramonte.wellapp.dto.output.PatientOutput;
import me.chiaramonte.wellapp.model.Patient;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface PatientMapper {

    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

    Patient patientInputToPatient(PatientInput dto);

    PatientOutput patientToPatientOutput(Patient p);

    List<Patient> patientInputListToPatientList(List<PatientInput> list);

}
