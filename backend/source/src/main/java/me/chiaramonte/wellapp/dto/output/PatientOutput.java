package me.chiaramonte.wellapp.dto.output;

import me.chiaramonte.wellapp.model.User;

import java.time.LocalDate;

public class PatientOutput extends User {

    private String uuid;

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate birthday;

    private String phone;

    private String picture;

}
