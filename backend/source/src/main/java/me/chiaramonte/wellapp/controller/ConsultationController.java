package me.chiaramonte.wellapp.controller;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.controllerExceptions.InvalidInputException;
import me.chiaramonte.wellapp.dto.input.ConsultationInput;
import me.chiaramonte.wellapp.model.Consultation;
import me.chiaramonte.wellapp.model.Patient;
import me.chiaramonte.wellapp.model.Symptom;
import me.chiaramonte.wellapp.service.ConsultationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("consultations")
public class ConsultationController {

    @Autowired
    private ConsultationService consultationService;

    /** Get One */

    @GetMapping("{uuid}")
    Consultation getConsultationByUuid(@PathVariable String uuid){
        return consultationService.findOneByUuid(uuid);
    }

    @GetMapping("{consultationUuid}/symptoms")
    List<Symptom> getConsultationSymptoms(@PathVariable String consultationUuid){
        return consultationService.findOneByUuid(consultationUuid).getSymptomList();
    }

    @GetMapping("{consultationUuid}/patient")
    Patient getConsultationPatient(@PathVariable String consultationUuid){
        return consultationService.findOneByUuid(consultationUuid).getPatient();
    }



    @GetMapping
    List<Consultation>getAllConsultations(){
        return consultationService.findAll();
    }

    /** Create */

    @PostMapping
    Consultation createConsultation(@RequestBody ConsultationInput consultationInput) throws InvalidInputException {
        return consultationService.create(consultationInput);
    }

    @PutMapping
    Consultation updateConsultation(@RequestBody ConsultationInput consultationInput) throws InvalidInputException {
        return consultationService.update(consultationInput);
    }






}
