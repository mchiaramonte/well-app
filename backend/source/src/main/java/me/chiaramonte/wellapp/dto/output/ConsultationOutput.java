package me.chiaramonte.wellapp.dto.output;

import me.chiaramonte.wellapp.model.Doctor;


import java.sql.Timestamp;

public class ConsultationOutput {

    private String uuid;

    private Timestamp startDateTime;

    private Timestamp endDateTime;

    private PatientOutput patient;

    private Doctor doctor;

}
