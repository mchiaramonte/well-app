package me.chiaramonte.wellapp.repository;

import me.chiaramonte.wellapp.model.ICDCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface ICDCategoryRepository extends CrudRepository<ICDCategory,Integer> {

    Optional<ICDCategory> findOneByUuid(String uuid);

    List<ICDCategory> findAllByCodeContainingIgnoreCaseOrDescriptionContainingIgnoreCase(@Param("code") String code, @Param("description") String description);

    @Query(value = "SELECT * FROM ICDCategory WHERE LOWER(code) like %:query% OR LOWER(description) like %:query% ORDER BY frequency DESC, code ASC LIMIT :numberOfResults", nativeQuery = true)
    List<ICDCategory> findByCodeOrDescCustomQuery(@Param("query") String query, @Param("numberOfResults") Integer numberOfResults);

    List<ICDCategory> findAll();

}
