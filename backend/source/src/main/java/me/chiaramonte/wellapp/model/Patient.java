package me.chiaramonte.wellapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.*;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Patient extends BaseEntity {

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate birthday;

    private String phone;

    @Hidden
    private String userRole;

    @ManyToOne
    private PatientAddress address;

    private String picture;

    // see for FetchType https://www.baeldung.com/hibernate-initialize-proxy-exception
    @JsonIgnore
    @OrderBy("startDateTime DESC")
    @OneToMany(mappedBy = "patient", fetch = FetchType.EAGER)
    private List<Consultation> consultationList;

}
