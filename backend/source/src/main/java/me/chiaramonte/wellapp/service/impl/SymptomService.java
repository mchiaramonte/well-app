package me.chiaramonte.wellapp.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.controllerExceptions.NotFoundException;
import me.chiaramonte.wellapp.dto.input.SymptomInput;
import me.chiaramonte.wellapp.mapper.SymptomMapper;
import me.chiaramonte.wellapp.model.Symptom;
import me.chiaramonte.wellapp.repository.SymptomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Slf4j
@Service
@Transactional
public class SymptomService {


    @Autowired
    private ICD10Service icd10Service;

    @Autowired
    private SymptomRepository symptomRepository;

    @Autowired
    private ConsultationServiceImpl consultationServiceImpl;

    @Autowired
    private SymptomMapper symptomMapper;

    public Symptom create(SymptomInput symptomInput){

        Symptom symptom = symptomMapper.symptomInputToSymptom(symptomInput);

        symptom.setConsultation(consultationServiceImpl.findOneByUuid(symptomInput.getConsultationUuid()));
        symptom.setIcdCategory(
                icd10Service
                        .findOneByUuid(symptomInput.getIcdCategoryUuid())
                        .orElseThrow(()->new NotFoundException("ICD10 Category not found"))
        );

        icd10Service.updateICD10CategoryFrequency(symptom.getIcdCategory(),1);



        return symptomRepository.save(symptom);
    }

    public Symptom findOneByUuid(String uuid){
        return symptomRepository.findOneByUuid(uuid).orElseThrow(()->new NotFoundException("Symptom not found"));
    }

    public List<Symptom> findAll(){
        return symptomRepository.findAll();
    }

    public Symptom delete(String symptomUuid) throws NotFoundException{


        Symptom symptom = findOneByUuid(symptomUuid);
        symptomRepository.deleteByUuid(symptomUuid);
        icd10Service.updateICD10CategoryFrequency(symptom.getIcdCategory(),-1);
        return symptom;

    }

}
