package me.chiaramonte.wellapp.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.controllerExceptions.NotFoundException;
import me.chiaramonte.wellapp.dto.input.PatientInput;
import me.chiaramonte.wellapp.mapper.PatientMapper;
import me.chiaramonte.wellapp.model.Consultation;
import me.chiaramonte.wellapp.model.Patient;
import me.chiaramonte.wellapp.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

// TODO Create separate interfaces and implementation classes
@Service
@Slf4j
@Transactional
public class PatientService {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private PatientMapper patientMapper;


    public Patient findOneByUuid(String uuid) throws NotFoundException {
        return patientRepository.findOneByUuid(uuid).orElseThrow(()->new NotFoundException("Patient not Found"));
    }

    public List<Patient> filterByFirstNameOrLastName(String query){
        return patientRepository.findByFirstNameOrLastNameCustom(query);
    }

    public List<Consultation> findPatientConsultations(String patientUuid){
        return findOneByUuid(patientUuid).getConsultationList();
    }


        private int getRandomNumber(int min, int max) {
        return (int) (Math.floor((Math.random() * (max - min)) + min));
    }

    public Iterable<Patient> createMockPatients(List<PatientInput> patientList){
        List<Patient> savedPatients = new ArrayList<>();


//        patientMapper.patientInputListToPatientList(patientList).forEach(p->{
//
//            Patient createdPatient = patientRepository.save(p);
//
//            savedPatients.add(createdPatient);
//
//
//        });
        return patientRepository.saveAll(patientMapper.patientInputListToPatientList(patientList));
    }



}
