package me.chiaramonte.wellapp.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.dto.input.ICDCategoryInput;
import me.chiaramonte.wellapp.mapper.ICDCategoryMapper;
import me.chiaramonte.wellapp.model.ICDCategory;
import me.chiaramonte.wellapp.repository.ICDCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Slf4j
@Service
@Transactional
public class ICD10Service {

    @Autowired
    private ICDCategoryRepository icdCategoryRepository;

    @Autowired
    private ICDCategoryMapper icdCategoryMapper;

    @Autowired
    private ConsultationServiceImpl consultationServiceImpl;

    @Autowired
    private SymptomService symptomService;

    public List<ICDCategory> findAll(){
        return icdCategoryRepository.findAll();
    }

    public List<ICDCategory> filter(String query, Integer size){
        return icdCategoryRepository.findByCodeOrDescCustomQuery(query.toLowerCase(),size);

    }

    public Integer createICD10Codes(List<ICDCategoryInput> icd10Codes){
        icdCategoryRepository.saveAll(icdCategoryMapper.icd10CategoryInputListToCategoryList(icd10Codes));
        return icdCategoryRepository.findAll().size();
    }


    public void updateICD10CategoryFrequency(ICDCategory icdCategory, Integer occurrencesToAdd){
        if(icdCategory.getFrequency()==null){
            icdCategory.setFrequency(1L);
        }else{
            icdCategory.setFrequency(icdCategory.getFrequency() + occurrencesToAdd);
        }
        icdCategoryRepository.save(icdCategory);
    }

    public Optional<ICDCategory> findOneByUuid(String icd10CategoryUuid) {
        return icdCategoryRepository.findOneByUuid(icd10CategoryUuid);
    }
}
