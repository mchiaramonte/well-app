package me.chiaramonte.wellapp.repository;

import me.chiaramonte.wellapp.model.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface PatientRepository extends CrudRepository<Patient,Long> {

    Optional<Patient> findOneByUuid(String uuid);

    List<Patient> findByFirstNameContainingIgnoreCaseOrLastNameContainingIgnoreCaseOrderByLastNameAsc(String firstName, String lastName);

    List<Patient> findAll();

    @Query(value = "SELECT p FROM Patient p WHERE LOWER(p.firstName) like %:query% OR LOWER(p.lastName) like %:query% ORDER BY p.lastName")
    List<Patient> findByFirstNameOrLastNameCustom(@Param("query") String query);
}
