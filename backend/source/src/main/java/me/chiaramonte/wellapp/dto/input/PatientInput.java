package me.chiaramonte.wellapp.dto.input;

import lombok.Getter;
import lombok.Setter;
import me.chiaramonte.wellapp.model.PatientAddress;

import java.time.LocalDate;

@Getter
@Setter
public class PatientInput  {

    private String uuid;

    private String firstName;

    private String lastName;

    private String email;

    private LocalDate birthday;

    private String phone;

    private String userRole;

//    private PatientAddress address;

    private String picture;

}
