package me.chiaramonte.wellapp.repository;

import me.chiaramonte.wellapp.model.Consultation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Repository
public interface ConsultationRepository extends CrudRepository<Consultation,Long> {

    Optional<Consultation> findOneByUuid(String uuid);

    List<Consultation> findAllByOrderByStartDateTimeDesc();

}
