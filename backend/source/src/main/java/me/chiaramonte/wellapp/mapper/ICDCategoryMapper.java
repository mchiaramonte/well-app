package me.chiaramonte.wellapp.mapper;

import me.chiaramonte.wellapp.dto.input.ICDCategoryInput;
import me.chiaramonte.wellapp.model.ICDCategory;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ICDCategoryMapper {

    ICDCategoryMapper INSTANCE = Mappers.getMapper(ICDCategoryMapper.class);

    List<ICDCategory> icd10CategoryInputListToCategoryList(List<ICDCategoryInput> dtoList);

}
