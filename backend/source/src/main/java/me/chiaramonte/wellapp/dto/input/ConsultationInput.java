package me.chiaramonte.wellapp.dto.input;

import lombok.*;

import java.time.ZonedDateTime;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ConsultationInput {

    private String uuid;

    private ZonedDateTime startDateTime;

    private ZonedDateTime endDateTime;

    private String patientUuid;

    private String doctorUuid;

}
