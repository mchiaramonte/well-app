package me.chiaramonte.wellapp.mapper;

import me.chiaramonte.wellapp.dto.input.ConsultationInput;
import me.chiaramonte.wellapp.model.Consultation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper
public interface ConsultationMapper {

    ConsultationMapper INSTANCE = Mappers.getMapper(ConsultationMapper.class);

    Consultation consultationInputToConsultation(ConsultationInput dto);


}
