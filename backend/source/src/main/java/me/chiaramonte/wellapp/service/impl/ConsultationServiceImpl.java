package me.chiaramonte.wellapp.service.impl;

import lombok.extern.slf4j.Slf4j;
import me.chiaramonte.wellapp.controllerExceptions.InvalidInputException;
import me.chiaramonte.wellapp.controllerExceptions.NotFoundException;
import me.chiaramonte.wellapp.dto.input.ConsultationInput;
import me.chiaramonte.wellapp.mapper.ConsultationMapper;
import me.chiaramonte.wellapp.model.Consultation;
import me.chiaramonte.wellapp.repository.ConsultationRepository;
import me.chiaramonte.wellapp.service.ConsultationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;


@Slf4j
@Transactional
@Service
public class ConsultationServiceImpl implements ConsultationService {

    @Autowired
    private PatientService patientService;

    @Autowired
    private ConsultationRepository consultationRepository;

    @Autowired
    private ConsultationMapper consultationMapper;

    @Override
    public Consultation findOneByUuid(String uuid) throws NotFoundException {
        return consultationRepository.findOneByUuid(uuid).orElseThrow(()->new NotFoundException("Consultation not Found"));
    }
    @Override
    public List<Consultation> findAll(){
        return consultationRepository.findAllByOrderByStartDateTimeDesc();
    }

    @Override
    public Consultation create(ConsultationInput consultationInput) throws InvalidInputException {
        try{
            Consultation consultation = consultationMapper.consultationInputToConsultation(consultationInput);
            consultation.setPatient(patientService.findOneByUuid(consultationInput.getPatientUuid()));

            return consultationRepository.save(consultation);
        }catch ( RuntimeException ex){
            log.info(ex.getMessage());
            throw new InvalidInputException();
        }
    }

    @Override
    public Consultation update(ConsultationInput consultationInput) throws InvalidInputException {
        try{
            Consultation oldConsultation = findOneByUuid(consultationInput.getUuid());

            oldConsultation.setStartDateTime(consultationInput.getStartDateTime());
            oldConsultation.setEndDateTime(consultationInput.getEndDateTime());

            return consultationRepository.save(oldConsultation);
        }catch ( RuntimeException ex){
            log.info(ex.getMessage());
            throw new InvalidInputException();
        }
    }

}
