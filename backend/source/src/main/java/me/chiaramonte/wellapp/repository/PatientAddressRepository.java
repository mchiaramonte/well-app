package me.chiaramonte.wellapp.repository;

import me.chiaramonte.wellapp.model.PatientAddress;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface PatientAddressRepository extends CrudRepository<PatientAddress,Long> {
}
