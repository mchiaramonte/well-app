package me.chiaramonte.wellapp.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
// @Access(AccessType.FIELD)
@MappedSuperclass
public class BaseEntity {

    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name="uuid2",strategy = "uuid2")
    @Id
    private String uuid;

}
