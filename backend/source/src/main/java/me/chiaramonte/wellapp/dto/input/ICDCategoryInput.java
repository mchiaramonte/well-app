package me.chiaramonte.wellapp.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ICDCategoryInput {

    private String code;

    private String description;

}
