package me.chiaramonte.wellapp.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.ZonedDateTime;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class Symptom extends BaseEntity {

    private String description;


    private ZonedDateTime occurrence;

    @ManyToOne
    private ICDCategory icdCategory;


    @JsonIgnore
    @ManyToOne
    private Consultation consultation;

}
