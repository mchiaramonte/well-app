package me.chiaramonte.wellapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class ApiError {

    private ZonedDateTime timestamp;
    private HttpStatus status;
    private String message;
    private List errors;

}
