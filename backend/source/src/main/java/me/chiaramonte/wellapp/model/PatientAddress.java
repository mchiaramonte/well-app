package me.chiaramonte.wellapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class PatientAddress extends BaseEntity {

    private String street;

    private String houseNumber;

    private String city;

    private String country;

    private Integer zipCode;

    @JsonIgnore
    @OneToMany(mappedBy = "address")
    private List<Patient> patientList;


}
