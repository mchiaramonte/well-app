package me.chiaramonte.wellapp.repository;

import me.chiaramonte.wellapp.model.Symptom;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SymptomRepository extends CrudRepository<Symptom,Long> {

    List<Symptom> findAll();

    Optional<Symptom> findOneByUuid(String uuid);

    void deleteByUuid(String uuid);

}
