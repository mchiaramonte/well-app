import { Component, OnInit } from '@angular/core';
export interface Section {
    name: string;
    updated: Date;
}
@Component({
    selector: 'app-main-dashboard',
    templateUrl: './main-dashboard.component.html',
    styleUrls: ['./main-dashboard.component.scss'],
})
export class MainDashboardComponent implements OnInit {
    constructor() {}

    ngOnInit(): void {}
}
