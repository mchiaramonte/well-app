import { Component, OnInit, Inject } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { addHours, addMinutes, format, getHours, startOfDay } from 'date-fns';
import {
    ConsultationControllerService,
    ConsultationInput,
    Patient,
    PatientControllerService,
} from 'src/api';

export interface DialogData {
    hoursSet: boolean;
    start: Date;
    end: Date;
}

export interface SlotDates {
    start: Date;
    end: Date;
}

@Component({
    selector: 'app-consultation-form',
    templateUrl: './consultation-form.component.html',
    styleUrls: ['./consultation-form.component.scss'],
})
export class ConsultationFormComponent implements OnInit {
    private consultationInput!: ConsultationInput;

    hours: Date[] = [];
    timeStepMinutes = 15;
    dates: SlotDates;
    defaultEventDurationMinutes = 60;
    patientList: Patient[] = [];
    selectedPatient!: Patient;

    constructor(
        private dialogRef: MatDialogRef<ConsultationFormComponent>,
        private consultationService: ConsultationControllerService,
        private patientService: PatientControllerService,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        this.generateTimeSteps();

        this.dates = {
            start: ConsultationFormComponent.roundToNextHour(new Date()),
            end: addHours(ConsultationFormComponent.roundToNextHour(new Date()), 1),
        };

        this.checkInputTypeAndSetHours(data);
    }

    ngOnInit(): void {
        this.patientService.filterPatientsByName('').subscribe({
            next: (list) => {
                this.patientList = list;
                if (list.length) {
                    this.selectedPatient = list[0];
                }
            },
        });
    }

    onSubmit() {
        // console.log('created');
        this.setupConsultationInput();
        this.consultationService.createConsultation(this.consultationInput).subscribe({
            next: (created) => {
                this.dialogRef.close(created);
            },
            error: (error) => {
                this.dialogRef.close();
            },
        });
    }

    private setupConsultationInput() {
        // console.log(this.selectedPatient);

        this.consultationInput.startDateTime = this.dates.start.toISOString();
        this.consultationInput.endDateTime = this.dates.end.toISOString();
        this.consultationInput.patientUuid = this.selectedPatient.uuid;
    }

    // TODO check if date is undefined
    changeDate(selectedDate: Date) {
        this.dates.start.setFullYear(selectedDate.getFullYear());
        this.dates.start.setMonth(selectedDate.getMonth());
        this.dates.start.setDate(selectedDate.getDate());

        this.dates.end.setFullYear(selectedDate.getFullYear());
        this.dates.end.setMonth(selectedDate.getMonth());
        this.dates.end.setDate(selectedDate.getDate());
    }

    selectStartTime(hour: Date) {
        this.dates.start = this.setMinutesAndHours(hour);
        if (this.dates.start >= this.dates.end) {
            this.dates.end = addMinutes(this.dates.start, this.timeStepMinutes);
        }
    }

    selectEndTime(hour: Date) {
        this.dates.end = this.setMinutesAndHours(hour);
        if (this.dates.start >= this.dates.end) {
            this.dates.start = addMinutes(this.dates.end, -this.timeStepMinutes);
        }
    }

    private setMinutesAndHours(date: Date) {
        date.setMinutes(date.getMinutes());
        date.setHours(date.getHours());
        return date;
    }

    /** Service methods */

    private generateTimeSteps() {
        const timeSteps = Math.floor((14 * 60) / this.timeStepMinutes);
        let coutingDate = addHours(startOfDay(new Date()), 6);

        for (let i = 0; i < timeSteps; i++) {
            this.hours.push(coutingDate);
            coutingDate = addMinutes(coutingDate, this.timeStepMinutes);
        }
    }

    private checkInputTypeAndSetHours(data: DialogData) {
        if (data !== null && data.start !== undefined) {
            if (!data.hoursSet) {
                this.changeDate(data.start);
            } else {
                this.dates.start = data.start;
                this.dates.end = addMinutes(data.start, this.defaultEventDurationMinutes);
            }
        }
        this.consultationInput = {
            startDateTime: this.dates.start.toUTCString(),
            endDateTime: this.dates.end.toISOString(),
            patientUuid: 'bla',
            doctorUuid: 'foo',
        };
    }

    private static roundToNextHour(date: Date) {
        date.setHours(date.getHours() + 1);
        date.setMinutes(0, 0, 0); // Resets also seconds and milliseconds
        return date;
    }
}
