import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SymptomSelectorComponent } from './symptom-selector.component';

describe('SymptomSelectorComponent', () => {
    let component: SymptomSelectorComponent;
    let fixture: ComponentFixture<SymptomSelectorComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [SymptomSelectorComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(SymptomSelectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
