import { IcdCategoryControllerService } from './../../../api/api/icdCategoryController.service';
import { ConsultationDashboardService } from './../../services/consultation-dashboard.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Patient, SymptomInput, Symptom, ICDCategory } from 'src/api';

@Component({
    selector: 'app-symptom-selector',
    templateUrl: './symptom-selector.component.html',
    styleUrls: ['./symptom-selector.component.scss'],
})
export class SymptomSelectorComponent implements OnInit, OnDestroy {
    patient!: Patient;
    defaultNumberOfSymptoms = 25;
    // consultation!: Consultation;
    cachedSearchedICD10Categories: ICDCategory[] = [];
    filteredICD10Categories: ICDCategory[] = [];

    /** Search Query Debounce Time */
    private lastChangeToSearchQuery: number = new Date().valueOf();
    private debounceSearchTimeMillis = 150;
    /** */
    selectedSymptoms: ICDCategory[] = [];

    availableSymptoms: ICDCategory[] = [];

    selectedICD10Uuids = new Set();

    icd10Search = '';

    constructor(
        private consultationDashboardService: ConsultationDashboardService,
        private icdService: IcdCategoryControllerService
    ) {}

    ngOnInit(): void {
        this.updateViewOnNewSymptoms();
        this.updateViewOnNewConsultation();
        this.searchICD10SymptomServerSide();
    }

    // TODO Fix BUG: not clearing up when leaving. Data persists when choosing new patient
    ngOnDestroy() {
        this.consultationDashboardService.consultationSubject.next({});
        this.consultationDashboardService.consultationSymptoms.next([]);
        this.selectedICD10Uuids = new Set();
    }

    updateViewOnNewConsultation() {
        this.consultationDashboardService.consultationSubject.subscribe(() => {
            this.selectedICD10Uuids.clear();
            this.selectedSymptoms = [];
            this.updateSelectedSymptomList([]);
        });
    }

    updateSelectedSymptomList(symptoms: Symptom[]) {
        symptoms
            .filter(
                (s) =>
                    s.icdCategory &&
                    s.icdCategory?.uuid &&
                    !this.symptomIsSelected(s.icdCategory?.uuid)
            )
            .forEach((s) => {
                this.selectedICD10Uuids.add(s?.icdCategory?.uuid);
                this.selectedSymptoms.push(Object.assign({}, s?.icdCategory));
            });
    }

    updateViewOnNewSymptoms() {
        this.consultationDashboardService.consultationSymptoms.subscribe((symptoms) => {
            if (symptoms) {
                this.updateSelectedSymptomList(symptoms);
            }
        });
    }

    removeAlreadySelectedICDFromSearchResults() {
        this.availableSymptoms = this.cachedSearchedICD10Categories.filter(
            (c) => !this.selectedICD10Uuids.has(c.uuid)
        );
    }

    symptomIsSelected(icd10Uuid: string): boolean {
        return this.selectedICD10Uuids.has(icd10Uuid);
    }

    /** Search  */
    searchICD10SymptomServerSide() {
        this.icdService.filter(this.icd10Search, this.defaultNumberOfSymptoms).subscribe({
            next: (icdFiltered: ICDCategory[]) => {
                this.cachedSearchedICD10Categories = new Array(...icdFiltered);
                this.removeAlreadySelectedICDFromSearchResults();
            },
        });
    }

    searchBoxChanged() {
        this.lastChangeToSearchQuery = new Date().valueOf();
        setTimeout(() => {
            if (
                Math.abs(new Date().valueOf() - this.lastChangeToSearchQuery) >
                this.debounceSearchTimeMillis
            ) {
                this.searchICD10SymptomServerSide();
            }
        }, this.debounceSearchTimeMillis);
    }

    drop(event: CdkDragDrop<ICDCategory[]>, newSymptom: boolean) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        } else {
            transferArrayItem(
                event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex
            );

            if (newSymptom) {
                this.selectedICD10Uuids.add(event.item.data.uuid);
                this.selectSymptom(event.item.data);
            } else {
                this.selectedICD10Uuids.delete(event.item.data.uuid);
                this.deleteSymptom(event.item.data);
            }
        }
    }

    /** CRUD */

    selectSymptom(icdCategory: ICDCategory) {
        const symptomInput: SymptomInput = {
            description: 'Description',
            occurrence: new Date().toISOString(),
            patientUuid: this.consultationDashboardService.patientSubject.getValue().uuid,
            consultationUuid: this.consultationDashboardService.consultationSubject.getValue().uuid,
            icdCategoryUuid: icdCategory.uuid,
        };
        // console.log('selecting symptom');

        this.consultationDashboardService.createSymptomForConsultation(symptomInput);
    }

    deleteSymptom(category: ICDCategory) {
        const symptomToDelete = this.consultationDashboardService.consultationSymptoms
            .getValue()
            .find((s) => s.icdCategory?.uuid === category.uuid);
        if (symptomToDelete) {
            this.consultationDashboardService.deleteSymptomConsultation(symptomToDelete);
        }
    }
}
