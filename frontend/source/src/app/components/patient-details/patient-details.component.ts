import { Component, OnInit } from '@angular/core';
import { ConsultationDashboardService } from 'src/app/services/consultation-dashboard.service';
import { Patient } from 'src/api';

@Component({
    selector: 'app-patient-details',
    templateUrl: './patient-details.component.html',
    styleUrls: ['./patient-details.component.scss'],
})
export class PatientDetailsComponent implements OnInit {
    constructor(public consultationDashboardService: ConsultationDashboardService) {}

    selectedPatient!: Patient;

    ngOnInit(): void {
        this.consultationDashboardService.patientSubject.subscribe(
            (patient) => (this.selectedPatient = patient)
        );
    }
}
