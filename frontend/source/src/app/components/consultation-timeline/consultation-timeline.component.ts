import { Component, OnInit } from '@angular/core';
import { ConsultationDashboardService } from 'src/app/services/consultation-dashboard.service';
import { Patient } from 'src/api';

@Component({
    selector: 'app-consultation-timeline',
    templateUrl: './consultation-timeline.component.html',
    styleUrls: ['./consultation-timeline.component.scss'],
})
export class ConsultationTimelineComponent implements OnInit {
    constructor(public consultationDashboardService: ConsultationDashboardService) {}

    patient!: Patient;

    ngOnInit(): void {
        this.consultationDashboardService.patientSubject.subscribe((patient) => {
            this.patient = patient;
        });
    }
}
