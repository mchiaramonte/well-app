import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { PatientControllerService } from '../../../api/api/patientController.service';
import { Patient } from '../../../api/model/patient';

@Component({
    selector: 'app-patient-list',
    templateUrl: './patient-list.component.html',
    styleUrls: ['./patient-list.component.scss'],
})
export class PatientListComponent implements OnInit {
    patients: Patient[] = [];
    searchQuery = '';

    /** Search Query Debounce Time */
    private lastChangeToSearchQuery: number = new Date().valueOf();
    private debounceSearchTimeMillis = 150;

    constructor(
        private patientControllerService: PatientControllerService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.searchPatientServerSide();
    }

    searchBoxChanged() {
        this.lastChangeToSearchQuery = new Date().valueOf();
        setTimeout(() => {
            if (
                Math.abs(new Date().valueOf() - this.lastChangeToSearchQuery) >
                this.debounceSearchTimeMillis
            ) {
                this.searchPatientServerSide();
            }
        }, this.debounceSearchTimeMillis);
    }

    searchPatientServerSide() {
        this.patientControllerService.filterPatientsByName(this.searchQuery.trim()).subscribe({
            next: (patients) => {
                this.patients = patients;
            },
        });
    }

    findConsultationAndNavigate(patient: Patient) {
        if (patient.uuid) {
            this.patientControllerService.getPatientConsultations(patient.uuid).subscribe({
                next: (consultationList) => {
                    //         // If no consultation is given, shows the last one

                    const filteredConsultations = consultationList.filter(
                        (c) => c.startDateTime && new Date(c.startDateTime) < new Date()
                    );

                    if (filteredConsultations.length) {
                        this.router.navigate([
                            '../patient',
                            patient.uuid,
                            'consultation',
                            filteredConsultations[0].uuid,
                        ]);
                    } else if (consultationList.length) {
                        this.router.navigate([
                            '../patient',
                            patient.uuid,
                            'consultation',
                            consultationList[0].uuid,
                        ]);
                    }
                },
            });
        }
    }
}
