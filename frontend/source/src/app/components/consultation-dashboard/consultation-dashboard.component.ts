import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConsultationDashboardService } from '../../services/consultation-dashboard.service';

@Component({
    selector: 'app-consultation-dashboard',
    templateUrl: './consultation-dashboard.component.html',
    styleUrls: ['./consultation-dashboard.component.scss'],
})
export class ConsultationDashboardComponent implements OnInit {
    constructor(
        private activatedRoute: ActivatedRoute,
        private consultationDashboardService: ConsultationDashboardService
    ) {
        this.consultationDashboardService.consultationSymptoms.next([]);
        this.consultationDashboardService.consultationSubject.next({});
        this.consultationDashboardService.patientSubject.next({});
        this.consultationDashboardService.consultationList.next([]);
    }

    ngOnInit(): void {
        this.activatedRoute.params.subscribe((paramsId) => {
            if (
                paramsId.patientUuid &&
                this.consultationDashboardService.patientSubject.getValue().uuid !==
                    paramsId.patientUuid
            ) {
                this.consultationDashboardService.loadConsultationsForPatient(paramsId.patientUuid);
                this.consultationDashboardService.loadPatient(paramsId.patientUuid);
            }

            if (paramsId.consultationUuid) {
                this.consultationDashboardService.loadConsultation(paramsId.consultationUuid);
            }
        });
    }
}
