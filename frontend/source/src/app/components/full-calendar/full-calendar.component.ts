import { Router } from '@angular/router';
import { ConsultationInput } from './../../../api/model/consultationInput';
import { addHours, getHours } from 'date-fns';
import { MatDialog } from '@angular/material/dialog';
import { AfterViewChecked, AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import {
    Calendar,
    CalendarOptions,
    DateInput,
    DatesSetArg,
    EventClickArg,
    EventInput,
    EventSourceInput,
    FullCalendarComponent,
} from '@fullcalendar/angular';
import {
    DateClickArg,
    EventDragStopArg,
    EventResizeDoneArg,
    EventResizeStopArg,
} from '@fullcalendar/interaction';
import { Consultation, ConsultationControllerService } from 'src/api';
import {
    ConsultationFormComponent,
    DialogData,
} from '../consultation-form/consultation-form.component';

export enum CalendarView {
    WEEK = 'timeGridWeek',
    DAY = 'timeGridDay',
    MONTH = 'dayGridMonth',
}

@Component({
    selector: 'app-full-calendar',
    templateUrl: './full-calendar.component.html',
    styleUrls: ['./full-calendar.component.scss'],
})
export class FullCalendarViewComponent implements OnInit, AfterViewInit {
    @ViewChild('fullcalendar')
    calendarComponent!: FullCalendarComponent;
    calendarApi!: Calendar;

    calendarOptions!: CalendarOptions;
    viewDate = new Date();
    viewOptions = CalendarView;
    viewType = 'Week';

    viewTypeMapping = {};

    calendarApiLoaded = false;

    constructor(
        public dialog: MatDialog,
        private consultationService: ConsultationControllerService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.setupCalendarOptions();
        this.loadConsultations();
    }

    ngAfterViewInit() {
        this.calendarApi = this.calendarComponent.getApi();
    }

    setupCalendarOptions() {
        this.calendarOptions = {
            initialView: 'timeGridWeek',
            dateClick: this.getDateFromClick.bind(this),
            headerToolbar: false,
            nowIndicator: true,
            allDaySlot: false,
            slotMinTime: '06:00:00',
            eventResize: this.resizedEvent.bind(this),
            eventDrop: this.rescheduledEvent.bind(this),
            eventClick: this.openEventInformation.bind(this),
            datesSet: this.changedDates.bind(this),
            height: 'auto',
            firstDay: 1,
            locale: 'de',
        };
    }

    changedDates(dateInfo: DatesSetArg) {
        if (this.calendarApi) {
            this.updateViewType();
            this.updateViewDate();
        }
    }

    getDateFromClick(info: DateClickArg) {
        this.openDialog(info.date, true);
    }

    loadConsultations() {
        this.consultationService.getAllConsultations().subscribe({
            next: (consultations) => {
                const events: EventInput[] = [];
                consultations.forEach((c) => {
                    events.push(this.convertToEventInputType(c));
                });
                this.calendarOptions.events = events;
            },
        });
    }

    convertToEventInputType(consultation: Consultation): EventInput {
        if (consultation.startDateTime && consultation.endDateTime) {
            return {
                start: new Date(consultation.startDateTime),
                end: new Date(consultation.endDateTime),
                title: 'Consultation',
                backgroundColor: '#120a64',
                editable: true,
                startEditable: true,
                durationEditable: true,
                overlap: true,
                extendedProps: { consultation: consultation },
            };
        } else {
            return {};
        }
    }

    openEventInformation(info: EventClickArg) {
        const consultationUuid = info.event.extendedProps.consultation.uuid;
        this.consultationService.getConsultationPatient(consultationUuid).subscribe({
            next: (patient) => {
                // console.log(patient);
                this.router.navigate([
                    'patient/' + patient.uuid + '/consultation/' + consultationUuid,
                ]);
            },
            error: (err) => {},
        });
    }

    resizedEvent(eventResizeInfo: EventResizeStopArg) {
        const consultationInput: ConsultationInput = Object.assign(
            {},
            eventResizeInfo.event.extendedProps.consultation
        );
        consultationInput.endDateTime = eventResizeInfo.event.end?.toISOString();

        this.updateConsultation(consultationInput);
    }

    rescheduledEvent(eventMoved: EventDragStopArg) {
        const consultationInput: ConsultationInput = Object.assign(
            {},
            eventMoved.event.extendedProps.consultation
        );
        consultationInput.startDateTime = eventMoved.event.start?.toISOString();
        consultationInput.endDateTime = eventMoved.event.end?.toISOString();
        this.updateConsultation(consultationInput);
    }

    updateConsultation(consultationInput: ConsultationInput) {
        this.consultationService.updateConsultation(consultationInput).subscribe({
            next: (updated) => {
                this.loadConsultations();
            },
            error: (err) => {
                this.loadConsultations();
            },
        });
    }

    createConsultation() {
        this.openDialog(new Date(), false);
    }

    openDialog(startHour: Date, hourSet: boolean) {
        const data: DialogData = {
            start: startHour,
            end: addHours(startHour, 1),
            hoursSet: hourSet,
        };
        this.dialog
            .open(ConsultationFormComponent, {
                data: data,
            })
            .afterClosed()
            .subscribe((saved: Consultation | undefined) => {
                if (saved) {
                    this.convertToEventInputType(saved);
                    this.loadConsultations();
                }
            });
    }

    updateViewType() {
        if (this.calendarApi.view.type === CalendarView.DAY) {
            this.viewType = 'Day';
        }
        if (this.calendarApi.view.type === CalendarView.WEEK) {
            this.viewType = 'Week';
        }
        if (this.calendarApi.view.type === CalendarView.MONTH) {
            this.viewType = 'Month';
        }
    }

    updateViewDate() {
        this.viewDate = this.calendarApi.getDate();
    }

    goToDate(date: Date) {
        this.calendarApi.gotoDate(date);
    }

    goToToday() {
        this.calendarApi.gotoDate(new Date());
    }
}
