import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FullCalendarViewComponent } from './full-calendar.component';

describe('FullCalendarComponent', () => {
    let component: FullCalendarViewComponent;
    let fixture: ComponentFixture<FullCalendarViewComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [FullCalendarViewComponent],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(FullCalendarViewComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
