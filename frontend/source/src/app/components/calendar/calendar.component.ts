import { Router } from '@angular/router';
import { DialogData } from './../consultation-form/consultation-form.component';
import { Subject } from 'rxjs';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarView,
} from 'angular-calendar';
import {
    addDays,
    startOfDay,
    subDays,
    endOfDay,
    isSameDay,
    isSameMonth,
    addHours,
    endOfMonth,
    parse,
    parseISO,
} from 'date-fns';
import { MatDialog } from '@angular/material/dialog';
import { ConsultationFormComponent } from '../consultation-form/consultation-form.component';
import { Consultation, ConsultationControllerService } from 'src/api';

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3',
    },
    blue: {
        primary: '#1e90ff',
        secondary: '#D1E8FF',
    },
    yellow: {
        primary: '#e3bc08',
        secondary: '#FDF1BA',
    },
    green: {
        primary: '#120a64',
        secondary: '#00be7d',
    },
};

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
})
export class CalendarComponent implements OnInit {
    dayStartingHour = 7;
    dayEndingHour = 18;

    constructor(
        public dialog: MatDialog,
        private consultationService: ConsultationControllerService,
        private router: Router
    ) {}

    ngOnInit() {
        this.loadConsultations();
    }

    @ViewChild('modalContent', { static: true })
    modalContent!: TemplateRef<any>;

    view: CalendarView = CalendarView.Week;

    calendarView = CalendarView;

    viewDate: Date = new Date();

    modalData!: {
        action: string;
        event: CalendarEvent;
    };

    actions: CalendarEventAction[] = [
        // {
        //   label: '<i class="fas fa-fw fa-pencil-alt"></i>',
        //   a11yLabel: 'Edit',
        //   onClick: ({ event }: { event: CalendarEvent }): void => {
        //     this.handleEvent('Edited', event);
        //   },
        // },
        // {
        //   label: '<mat-icon>delete</mat-icon>',
        //   a11yLabel: 'Delete',
        //   onClick: ({ event }: { event: CalendarEvent }): void => {
        //     this.events = this.events.filter((iEvent) => iEvent !== event);
        //     this.handleEvent('Deleted', event);
        //   },
        // },
    ];

    /** Publish a next() to update the calendar content */
    refresh: Subject<any> = new Subject();

    events: CalendarEvent[] = [];

    activeDayIsOpen = true;

    loadConsultations() {
        this.events = [];

        this.consultationService.getAllConsultations().subscribe({
            next: (consultations) => {
                consultations.forEach((c) => {
                    this.addEventToList(c);
                });
                this.refresh.next();
            },
        });
    }

    addEventToList(consultation: Consultation): void {
        if (consultation.startDateTime && consultation.endDateTime) {
            this.events.push({
                start: new Date(consultation.startDateTime),
                end: new Date(consultation.endDateTime),
                title: 'Consultation',
                color: colors.green,
                actions: this.actions,
                resizable: {
                    beforeStart: false,
                    afterEnd: false,
                },
                draggable: false,
                cssClass: '{background: red}',
            });
        }
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        this.viewDate = date;
        this.view = CalendarView.Day;
    }

    eventTimesChanged({ event, newStart, newEnd }: CalendarEventTimesChangedEvent): void {}

    handleEvent(action: string, event: CalendarEvent): void {
        // console.log(event, action);
        // this.modalData = { event, action };
    }

    deleteEvent(eventToDelete: CalendarEvent) {
        this.events = this.events.filter((event) => event !== eventToDelete);
    }

    setView(view: CalendarView) {
        this.view = view;
    }

    createConsultation() {
        this.openDialog(new Date(), false);
    }

    selectedHourSegment(date: Date) {
        this.openDialog(date, true);
    }

    openDialog(startHour: Date, hourSelected: boolean) {
        const data: DialogData = {
            start: hourSelected ? startHour : this.viewDate,
            end: this.viewDate,
            hoursSet: hourSelected,
        };
        this.dialog
            .open(ConsultationFormComponent, {
                data: data,
            })
            .afterClosed()
            .subscribe((saved: Consultation | undefined) => {
                if (saved) {
                    this.addEventToList(saved);
                    this.refresh.next();
                }
            });
    }

    selectConsultation(action: string, event: CalendarEvent) {
        this.router.navigateByUrl('/consultation');
    }
}
