import { Component, OnInit } from '@angular/core';

export enum PressureRange {
    TOP,
    BOTTOM,
}

@Component({
    selector: 'app-vital-signs',
    templateUrl: './vital-signs.component.html',
    styleUrls: ['./vital-signs.component.scss'],
})
export class VitalSignsComponent implements OnInit {
    pressureRangeBottom = 80;
    pressureRangeTop = 120;
    pressureRange = PressureRange;
    pressureStep = 5;
    temperature = 36.6;
    temperatureStep = 0.2;
    bloodSugar = 80;
    bloodSugarStep = 10;
    constructor() {}

    ngOnInit(): void {}

    formatLabel(value: number) {
        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }

        return value;
    }

    pressureUp(range: PressureRange) {
        if (range === PressureRange.TOP) {
            this.pressureRangeTop = this.pressureRangeTop + this.pressureStep;
        } else {
            this.pressureRangeBottom = this.pressureRangeBottom + this.pressureStep;
        }
    }

    pressureDown(range: PressureRange) {
        if (range === PressureRange.TOP) {
            this.pressureRangeTop = this.pressureRangeTop - this.pressureStep;
        } else {
            this.pressureRangeBottom = this.pressureRangeBottom - this.pressureStep;
        }
    }
}
