import { Consultation, ConsultationControllerService } from 'src/api';
import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-upcoming-consultations',
    templateUrl: './upcoming-consultations.component.html',
    styleUrls: ['./upcoming-consultations.component.scss'],
})
export class UpcomingConsultationsComponent implements OnInit {
    consultations: Consultation[] = [];
    constructor(public consultationService: ConsultationControllerService) {}

    ngOnInit(): void {
        this.loadConsultations();
    }

    loadConsultations() {
        this.consultationService.getAllConsultations().subscribe({
            next: (c) => {
                this.consultations = c
                    .filter(this.filterFutureConsults)
                    .sort(this.sortFunction)
                    .slice(0, 6);
            },
        });
    }

    filterFutureConsults(c: Consultation) {
        if (c.startDateTime) {
            return new Date(c.startDateTime) > new Date();
        } else return false;
    }

    sortFunction(a: Consultation, b: Consultation) {
        if (a.startDateTime && b.startDateTime) {
            return new Date(a.startDateTime).getTime() - new Date(b.startDateTime).getTime();
        } else return -1;
    }
}
