import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ApiModule, Configuration } from 'src/api';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SymptomSelectorComponent } from './components/symptom-selector/symptom-selector.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatChipsModule } from '@angular/material/chips';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { PatientDetailsComponent } from './components/patient-details/patient-details.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { MatSliderModule } from '@angular/material/slider';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';

import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarComponent } from './components/calendar/calendar.component';

import { ConsultationTimelineComponent } from './components/consultation-timeline/consultation-timeline.component';
import { ConsultationDashboardComponent } from './components/consultation-dashboard/consultation-dashboard.component';
import { ConsultationFormComponent } from './components/consultation-form/consultation-form.component';
import { MainDashboardComponent } from './components/main-dashboard/main-dashboard.component';
import { PatientListComponent } from './components/patient-list/patient-list.component';
import { FullCalendarViewComponent } from './components/full-calendar/full-calendar.component';
import { VitalSignsComponent } from './components/vital-signs/vital-signs.component';
import { UpcomingConsultationsComponent } from './components/upcoming-consultations/upcoming-consultations.component';

import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import deLocale from '@fullcalendar/core/locales/de';

FullCalendarModule.registerPlugins([
    // register FullCalendar plugins
    dayGridPlugin,
    interactionPlugin,
    timeGridPlugin,
    bootstrapPlugin,
]);

@NgModule({
    declarations: [
        AppComponent,
        SymptomSelectorComponent,
        ConsultationDashboardComponent,
        ConsultationTimelineComponent,
        PatientDetailsComponent,
        CalendarComponent,
        ConsultationFormComponent,
        MainDashboardComponent,
        PatientListComponent,
        FullCalendarViewComponent,
        VitalSignsComponent,
        UpcomingConsultationsComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatInputModule,
        MatFormFieldModule,
        FormsModule,
        MatIconModule,
        MatChipsModule,
        DragDropModule,
        MatSidenavModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatDialogModule,
        MatSelectModule,
        MatListModule,
        MatCardModule,
        MatSliderModule,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
        FullCalendarModule,

        ApiModule.forRoot(() => {
            /** open-api-generator does not set basePath automatically*/
            return new Configuration({
                basePath: 'api/v1',
            });
        }),
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
