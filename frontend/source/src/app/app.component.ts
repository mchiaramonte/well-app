import { Component } from '@angular/core';
import { Router } from '@angular/router';

export interface SideMenu {
    name: string;
    route: string;
    icon: string;
    image: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'well-app';
    sideBarDisplayed = true;

    constructor(private router: Router) {}

    ngOnInit() {
        // this.accountService.userSubject.subscribe((user) => {
        //   if (user.userToken) {
        //     this.user = user;
        //     this.sideBarDisplayed = true;
        //   } else {
        //     this.sideBarDisplayed = false;
        //     this.router.navigateByUrl('login');
        //   }
        // });
    }

    menus: SideMenu[] = [
        {
            name: 'Consultation',
            route: 'consultation',
            icon: '',
            image: 'assets/img/stethoscope.png',
        },
        {
            name: 'Users',
            route: 'schedule2',
            icon: 'date_range',
            image: '',
        },
        //
    ];

    navigateToMenu(menu: any) {
        this.router.navigateByUrl(menu.route);
    }

    logout() {
        // this.accountService.logout();
    }
}
