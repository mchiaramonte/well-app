import { FullCalendarViewComponent } from './components/full-calendar/full-calendar.component';
import { MainDashboardComponent } from './components/main-dashboard/main-dashboard.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultationDashboardComponent } from './components/consultation-dashboard/consultation-dashboard.component';
import { PatientListComponent } from './components/patient-list/patient-list.component';

const routes: Routes = [
    { path: '', redirectTo: 'consultation', pathMatch: 'full' },
    {
        path: 'consultation',
        children: [
            {
                path: '',
                component: MainDashboardComponent,
            },
        ],
    },
    {
        path: 'patient',
        children: [
            {
                path: ':patientUuid',
                children: [
                    {
                        path: 'consultation/:consultationUuid',
                        component: ConsultationDashboardComponent,
                    },
                ],
            },
        ],
    },

    { path: 'schedule', component: CalendarComponent },
    { path: 'schedule2', component: FullCalendarViewComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
