import { PatientControllerService } from './../../api/api/patientController.service';
import { ConsultationControllerService } from './../../api/api/consultationController.service';
import { Symptom } from './../../api/model/symptom';
import { Patient } from './../../api/model/patient';
import { Consultation } from './../../api/model/consultation';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SymptomControllerService, SymptomInput } from 'src/api';

@Injectable({
    providedIn: 'root',
})
export class ConsultationDashboardService {
    patientSubject: BehaviorSubject<Patient> = new BehaviorSubject({});
    consultationSubject: BehaviorSubject<Consultation> = new BehaviorSubject({});
    consultationSymptoms: BehaviorSubject<Symptom[]> = new BehaviorSubject(new Array());
    consultationList: BehaviorSubject<Consultation[]> = new BehaviorSubject(new Array());

    constructor(
        private consultationService: ConsultationControllerService,
        private patientService: PatientControllerService,
        private symptomService: SymptomControllerService
    ) {
        this.consultationSubject.subscribe((selectedConsultation) => {
            if (selectedConsultation) {
                this.loadConsultationSymptoms(selectedConsultation);
            }
        });
    }

    loadPatient(patientUuid: string) {
        this.patientService.getPatientByUuid(patientUuid).subscribe({
            next: (patient) => {
                this.patientSubject.next(patient);
            },
            error: (err) => {
                console.debug(err);
            },
        });
    }

    loadConsultationsForPatient(patientUuid: string) {
        this.patientService.getPatientConsultations(patientUuid).subscribe({
            next: (consultations) => {
                this.consultationList.next(consultations);
            },
        });
    }

    loadConsultation(consultationUuid: string) {
        this.consultationService.getConsultationByUuid(consultationUuid).subscribe({
            next: (consultation) => {
                this.consultationSubject.next(consultation);
            },
        });
    }

    loadConsultationSymptoms(consultation: Consultation) {
        if (consultation.uuid) {
            this.consultationService.getConsultationSymptoms(consultation.uuid).subscribe({
                next: (symptoms) => {
                    this.consultationSymptoms.next(symptoms);
                },
                error: (err) => {
                    console.debug(err);
                },
            });
        }
    }

    subscribeToConsultationSubjectChanges() {
        this.consultationSubject.subscribe((consultation) => {
            this.loadConsultationSymptoms(consultation);
        });
    }

    setConsultation(consultation: Consultation) {
        this.consultationSubject.next(consultation);
    }

    createSymptomForConsultation(symptomInput: SymptomInput) {
        this.symptomService.createSymptom(symptomInput).subscribe({
            next: (createdSymptom) => {
                this.loadConsultationSymptoms(this.consultationSubject.getValue());
            },
            error: (err) => {
                console.debug(err);
            },
        });
    }

    deleteSymptomConsultation(symptom: Symptom) {
        if (symptom.icdCategory && symptom.uuid) {
            this.symptomService.deleteSymptom(symptom.uuid).subscribe({
                next: () => {
                    this.loadConsultationSymptoms(this.consultationSubject.getValue());
                },
            });
        }
    }
}
