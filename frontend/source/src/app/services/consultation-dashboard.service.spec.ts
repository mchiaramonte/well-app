import { TestBed } from '@angular/core/testing';

import { ConsultationDashboardService } from './consultation-dashboard.service';

describe('ConsultationDashboardService', () => {
    let service: ConsultationDashboardService;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ConsultationDashboardService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
