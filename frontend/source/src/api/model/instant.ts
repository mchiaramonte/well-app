/**
 * Well App Demo
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { Chronology } from './chronology';
import { DateTimeZone } from './dateTimeZone';


export interface Instant { 
    chronology?: Chronology;
    millis?: number;
    zone?: DateTimeZone;
    afterNow?: boolean;
    beforeNow?: boolean;
    equalNow?: boolean;
}

