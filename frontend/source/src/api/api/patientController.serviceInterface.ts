/**
 * Well App Demo
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { ApiError } from '../model/models';
import { Consultation } from '../model/models';
import { Patient } from '../model/models';
import { PatientInput } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface PatientControllerServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * 
     * 
     * @param patientInput 
     */
    create(patientInput: Array<PatientInput>, extraHttpRequestParams?: any): Observable<object>;

    /**
     * 
     * 
     * @param query 
     */
    filterPatientsByName(query?: string, extraHttpRequestParams?: any): Observable<Array<Patient>>;

    /**
     * 
     * 
     * @param patientUuid 
     */
    getPatientByUuid(patientUuid: string, extraHttpRequestParams?: any): Observable<Patient>;

    /**
     * 
     * 
     * @param patientUuid 
     */
    getPatientConsultations(patientUuid: string, extraHttpRequestParams?: any): Observable<Array<Consultation>>;

}
