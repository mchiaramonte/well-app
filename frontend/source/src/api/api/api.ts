export * from './consultationController.service';
import { ConsultationControllerService } from './consultationController.service';
export * from './consultationController.serviceInterface'
export * from './icdCategoryController.service';
import { IcdCategoryControllerService } from './icdCategoryController.service';
export * from './icdCategoryController.serviceInterface'
export * from './patientController.service';
import { PatientControllerService } from './patientController.service';
export * from './patientController.serviceInterface'
export * from './status.service';
import { StatusService } from './status.service';
export * from './status.serviceInterface'
export * from './symptomController.service';
import { SymptomControllerService } from './symptomController.service';
export * from './symptomController.serviceInterface'
export const APIS = [ConsultationControllerService, IcdCategoryControllerService, PatientControllerService, StatusService, SymptomControllerService];
